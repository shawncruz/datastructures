public abstract class BinaryHeap<E> implements PriorityQueueADT<E>
{
	protected Object[] array;
	protected int count;
	protected int capacity;
	private static final int DEF_CAP = 10; // Default capacity

	/*
	*	Constructor with user-specified capacity
	*/
	public BinaryHeap(int capacity)
	{
		array = new Object[capacity];
		this.capacity = capacity;
		count = 0;
	}
	/*
	*	Constructor with default capacity
	*/
	public BinaryHeap()
	{
		this(DEF_CAP);
	}

	/*
	*	Get index of parent
	*/
	protected int parent(int index)
	{
		if(index <= 0 && index >= count)
			return Integer.MIN_VALUE;

		return (index - 1) / 2;
	}

	/*
	*	Get index of left child
	*/
	protected int leftChild(int index)
	{
		int left = (index * 2) + 1;

		if(left >= count || left < 0)
			return Integer.MIN_VALUE;

		return left;
	}

	/*
	*	Get index of right child
	*/
	protected int rightChild(int index)
	{
		int right = (index * 2) + 2;

		if(right >= count || right < 0)
			return Integer.MIN_VALUE;

		return right;
	}

	/*
	*	Swap elements at specified indices
	*/
	protected void swap(int pos1, int pos2)
	{
		E temp = array[pos1];
		array[pos1] = array[pos2];
		array[pos2] = temp;
	}

	// For Heapify
	protected abstract E insert(E e);
	protected abstract void percolateDown(int index);
	protected abstract void percolateUp(int index);
}