public interface MinPriorityQueueADT<E> extends PriorityQueueADT<E>
{
	E deleteMin();
	E getMin();
}