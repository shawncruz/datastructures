public interface BinarySearchTreeADT<E extends Comparable<? super E>> extends BinaryTreeADT<E>
{
	E findMin(); // Finds min elemeht in tree
	E findMax(); // Finds max element in tree
	E delete(E e); // Delete an element from the tree
	//E kthSmallest(); // Find kth smallest element in tree
}