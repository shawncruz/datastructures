public interface BinaryTreeADT<E extends Comparable<? super E>>
{
	void insert(E e); // insert element into tree
	//E remove(E e); // delete and return ewlement from tree
	E find(E e); // find element in tree
	void preorder(); // preorder traversal (DFS)
	void postorder(); // postorder traversal (DFS)
	void inorder(); // inorder traversal (DFS)
	void levelorder(); // levelorder (BFS) traversal
}