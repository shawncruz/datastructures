public class SinglyLinkedList<E> implements ListADT<E>
{
	private Node<E> head;

	/*******************************************************
	*
	*  The Node class
	*
	********************************************************/
	private static class Node<E>
	{
		private E e;
		private Node<E> next;

		public Node(E e)
		{
			this.e = e;
		}
	}

	// Create list without elements
	public SinglyLinkedList()
	{
		head = null;
	}
	// Create list with a default element
	public SinglyLinkedList(E e)
	{
		head = new Node<>(e);
	}

	/*
	** Add element to end of list.
	** Get rid of this in Interface.
	*/
	public void add(E e)
	{
		addLast(e);
	}
	// Add element to beginning of the list
	public void addFirst(E e)
	{
		add(1, e);
	}
	// Add element to the end of the list
	public void addLast(E e)
	{
		Node<E> node  = new Node<E>(e);
		// List is empty
		if(isEmpty())
		{
			head = node;
		}
		else
		{
			Node<E> runner = head;
			// Traverse to second to last element
			while(runner.next != null)
				runner = runner.next;
			// Add to after last element
			runner.next = node;
		}
	}

	// Add element at specfic index
	public void add(int index, E e)
	{
		Node<E> node  = new Node<E>(e);
		if(isEmpty()) // List is empty
		{
			head = node;
		}
		else if(index == 1) // Insert at front
		{
			node.next = head;
			head = node;
		}
		else // Insert elsewhere
		{
			Node<E> runner = head;
			while(index - 1 > 1 && runner != null)
			{
				runner = runner.next;
				index--;
			}

			if(runner.next == null) // End
			{
				runner.next = node;
			}
			else
			{
				node.next = runner.next.next;
				runner.next = node;
			}
		}
	}
	// Clear entire list
	public void clear()
	{
		head = null;
	}
	// Return element at index
	public E get(int index)
	{
		Node<E> runner = head;
		while(index != 1 && runner != null)
		{
			runner = runner.next;
		}
		return runner.e;
	}
	// Return index of element
	public int indexOf(Object o)
	{
		return -1;
	}
	// Remove and return element at index
	public E remove(int index)
	{
		E e;
		if(index == 1)
		{
			e = removeFirst();
		}
		else
		{
			Node<E> runner = head;
			while(index - 1 != 1 && runner != null)
			{
				runner = runner.next;
				index--;
			}

			e = runner.next.e; // Value to return
			if(runner.next.next == null) // Last element
				runner.next = null;
			else // Middle
				runner.next = runner.next.next;
		}

		return e;

	}
	// Remove and return first element
	public E removeFirst()
	{
		Node<E> first = head;
		E e = first.e;
		head = first.next;
		return e;
	}
	// Set element at index to value e
	public E set(int index, E e)
	{
		Node<E> runner = head;
		while(index != 1 && runner != null)
		{
			runner = runner.next;
			index--;
		}
		runner.e = e;
		return e;
	}
	public boolean isEmpty()
	{
		return head == null;
	}
	// Return the size of the list
	public int size()
	{
		int size = 0;
		Node<E> runner = head;
		while(runner != null)
		{
			runner = runner.next;
			size++;
		}

		return size;
	}
	// Print contents of the list starting from head
	public void print()
	{
		if(isEmpty()) return;
		Node<E> runner = head;
		System.out.print(
			"[ " + runner.e + " ]");
		while(runner.next != null)
		{
			runner = runner.next;
			System.out.print(
				" -> [ " + runner.e + " ]");
		}
		System.out.println(); // Newline
	}
}