public class MaxBinaryHeap<E> extends BinaryHeap<E> implements MaxPriorityQueueADT<E>
{
	/*
	*	Constructor with user-specified capacity
	*/
	public MaxBinaryHeap(int capacity)
	{
		super(capacity);
	}

	/*
	*	Insert element
	*/
	public E insert(E element)
	{
		if(count == capacity) // Ensure capacity
			resize();
		array[count] = element;
		percolateUp(count++);
		return element;
	}

	/*
	*	Resize array
	*/
	private void resize(E element)
	{
		capacity *= 2; // Possibly bad readability
		Object[] temp = new Object[capacity];
		for(int i = 0; i < count; temp[i] = array[i], i++);
		array = temp;
	}

	/*
	*	Return the maximum element in the heap
	*/
	public E getMax()
	{
		if(count == 0)
			return null;

		return (E) array[0];
	}

	/*
	*	Remove and return the maximum element in the heap
	*/
	public E deleteMax()
	{
		if(count == 0)
			return null;

		E max = array[0];
		array[0] = array[count - 1];
		count--;
		percolateDown(0);
		return max;
	}

	/*
	*	Percolate element downward
	*/
	protected void percolateDown(int index)
	{
		// Index of largest element
		int max = index;
		// Right and left indices
		int left = leftChild(index);
		int right = rightChild(index);

		if(left != Integer.MIN_VALUE && array[left].compareTo(array[index]) == 1)
			max = left;
		else if(right != Integer.MIN_VALUE && array[right].compareTo(array[index]) == 1)
			max = right;

		if(max != index) // Swap elements
		{
			E temp = array[index];
			array[index] = array[max];
			array[max] = temp;
		}
		else return; // Node properly set
		// Need to return at some point
		percolateDown(max);
	}

	/*
	*	Percolate element upward
	*/
	protected void percolateUp(int index)
	{
		int parent = parent(index);
		while(parent != Integer.MAX_VALUE)
		{
			if(array[index].compareTo(array[parent]) == 1)
				swap(index, parent);
			parent = parent(parent);
		}
	}
}