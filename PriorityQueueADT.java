public interface PriorityQueueADT<E>
{
	E insert(E e);
}