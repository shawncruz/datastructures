public interface QueueADT<E>
{
	void enqueue(E e);
	E dequeue();
	E front();
	int size();
	boolean isEmpty();
}