public class DNode<E>
{
	DNode<E> next;
	DNode<E> prev;
	E e;
	public DNode(E e)
	{
		this.e = e;
	}
}