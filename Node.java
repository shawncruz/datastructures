public class Node<E>
{
	Node<E> next;
	E e;
	public Node(E e)
	{
		this.e = e;
	}
}