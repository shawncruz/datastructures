public interface ListADT<E>
{
   void add(E e);
   void add(int index, E e);
   void clear();
   abstract E get(int index);
   int indexOf(Object o);
   E remove(int index);
   E set(int index, E e);
}
