/*
** Uses an array as the underlying data structure.
** Dynamically resizes when capacity is reached.
*/
public class ArrayStack<E> implements StackADT<E>
{
	// Could use ArrayList implementation for stack type.
	private Object[] stack;
	private int elementCount;
	/*
	** Creates an stack with array of size 10.
	*/
	public ArrayStack()
	{
		this(10);
	}
	/*
	** Creates an stack with array specified size.
	*/
	public ArrayStack(int size)
	{
		stack = new Object[size];
		elementCount = 0;
	}

	/*
	** Add new element to next available position in array.
	*/
	public void push(E e)
	{
		// Ensure capacity in array
		ensureCapacity();
		stack[elementCount++] = e;
	}
	/*
	** Remove and return the last element from the array.
	*/
	public E pop()
	{
		if(isEmpty()) return null; // Nothing to pop
		// Else
		return (E) stack[--elementCount];
	}
	/*
	** Return the last (top) element from the array.
	*/
	public E top()
	{
		if(isEmpty()) return null; // Nothing to peak
		// Else
		return (E) stack[elementCount - 1];
	}
	/*
	** Return number of elements in stack.
	*/
	public int size()
	{
		return elementCount;
	}
	/*
	** Ensure array is not empty.
	*/
	public boolean isEmpty()
	{
		return elementCount == 0;
	}
	/*
	** Ensure array is not full.
	*/
	private void ensureCapacity()
	{
		if(stack.length - 1 < elementCount)
			resize();
	}
	// Resizes list to twice the amount of elements in the list.
	private void resize()
	{
		Object[] copy = stack;
		stack = new Object[2*elementCount];
		for(int i = 0; i < copy.length; i++)
			stack[i] = copy[i];
	}
	/*
	** Print contents of stack top-down.
	*/
	public void print()
	{
		for(int i = elementCount - 1; i >= 0; i--)
		{
			System.out.println("--------------------");
			System.out.println("|     " + stack[i] + "     |");
		}
		System.out.println("--------------------");
	}
}