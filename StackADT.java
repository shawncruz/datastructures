public interface StackADT<E>
{
	void push(E e);
	E pop();
	boolean isEmpty();
}