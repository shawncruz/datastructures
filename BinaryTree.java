public class BinaryTree<E1 extends E> implements BinaryTreeADT<E>
{
	public static void main(String[] args)
	{
		BinaryTree<Integer> bt = new BinaryTree<>();
		System.out.println(bt.isEmpty());
		bt.insert(1);
		bt.insert(2);
		bt.insert(3);
		bt.insert(4);
		bt.insert(5);
		bt.levelorder();
		System.out.println();
	}

	protected BTNode<E> root; // root of the instance

	/*******************************************************
	*
	*  The BTNode (inner)class
	*
	********************************************************/
	protected static class BTNode<E extends Comparable<E>>
	{
		protected E data;
		protected BTNode<E> left;
		protected BTNode<E> right;

		public BTNode(E data)
		{
			this.data = data;
		}
	}

	public boolean isEmpty()
	{
		return (root == null);
	}

	/*******************************************************
	*
	*  Insertion/Find
	*
	********************************************************/
	public void insert(E data)
	{
		root = insert(root, data); // insert starting from root
	}

	/*
	* Inserts new element at node
	*/
	private BTNode insert(BTNode node, E data)
	{
		if(node == null)
		{
			node = new BTNode(data);
		}
		else
		{
			if(node.right == null)
				node.right = insert(node.right, data);
			else
				node.left = insert(node.left, data);
		}
		return node;
	}

	/*******************************************************
	*
	*  Traversals using iterative implementations
	*
	********************************************************/
	// Preorder traversal (DFS)
	public void preorder()
	{
		preorder(root); // this allows root to be a treated like a local variable
	}
	private void preorder(BTNode root)
	{
		StackADT<BTNode> stack = new ArrayStack<>();
		// Go until stack is empty
		while(true)
		{
			while(root != null)
			{
				print(root);
				stack.push(root);
				root = root.left;
			}
			if(stack.isEmpty())
				break;
			root = stack.pop();
			root = root.right;
		}
	}
	// Postorder traversal (DFS)
	public void postorder()
	{
		postorder(root);
	}
	// FIX: NOT WORKING CORRECTLY!
	private void postorder(BTNode root)
	{
		StackADT<BTNode> stack = new ArrayStack<>();
		E data = null; // This is required to not get a null pointer
		BTNode prev = new BTNode(data);
		stack.push(root);
		// Go until stack is empty
		while(!stack.isEmpty())
		{
			BTNode current = stack.pop();
			if(prev != null
				|| prev.left == current
				|| prev.right == current)
			{
				if(current.left != null)
				{
					stack.push(current.left);
				}
				else if(current.right != null)
				{
					stack.push(current.right);
				}
			}
			else if(current.left == prev) // No more left nodes
			{
				// There exists a non-null right node
				if(current.right != null)
					stack.push(current.right);
			}
			else // No more right nodes
			{
				System.out.println("top");
				print(current);
				stack.pop(); // Pop into the ether
			}
			prev = current;
		}
	}
	// Inorder traversal (DFS)
	public void inorder()
	{
		inorder(root);
	}
	public void inorder(BTNode root)
	{
		StackADT<BTNode> stack = new ArrayStack<>();
		// Go until stack is empty
		while(true)
		{
			while(root != null)
			{
				stack.push(root);
				root = root.left;
			}
			if(stack.isEmpty())
				break;
			root = stack.pop();
			print(root);
			root = root.right;
		}
	}
 	// Levelorder (BFS) traversal using a queue
 	public void levelorder()
	{
		levelorder(root);
	}
	public void levelorder(BTNode root)
	{
		ArrayQueue<BTNode> q = new ArrayQueue<>(); // Make QueueADT more NSYNC
		BTNode node;
		q.enqueue(root);
		while(!q.isEmpty())
		{
			node = q.dequeue();
			print(node);
			if(node.left != null)
				q.enqueue(node.left);
			if(node.right != null)
				q.enqueue(node.right);
		}
	}

	public E find(E data)
	{
		return null;
	}

	// Auxiliary method for printlns on BTNodes
	private void print(BTNode node)
	{
		System.out.println("node: " + node.data);
	}
}