public class DoublyLinkedList<E> implements ListADT<E>
{
	private DNode<E> head;

	/*******************************************************
	*
	*  The DNode class
	*
	********************************************************/
	private static class DNode<E>
	{
		private E e;
		private DNode<E> next;
		private DNode<E> prev;

		public DNode(E e)
		{
			this.e = e;
		}
	}

	public DoublyLinkedList()
	{
		head = null;
	}

	public DoublyLinkedList(E e)
	{
		head = new DNode<E>(e);
	}

	// Add element to end of list.
	// Get rid of this in Interface.
	public void add(E e)
	{
		addLast(e);
	}
	// Add element to beginning of the list
	public void addFirst(E e)
	{
		add(1, e); // 1 is the beginning of the list
	}
	// Add element to the end of the list
	// Could use a tail i.v., but not for now.
	public void addLast(E e)
	{
		DNode<E> node  = new DNode<E>(e);
		// List is empty
		if(isEmpty())
		{
			head = node;
		}
		else
		{
			DNode<E> runner = head;
			// Traverse to second to last element
			while(runner.next != null)
				runner = runner.next;
			// Add to after last element
			runner.next = node;
			node.prev = runner;
		}
	}

	/*
	** Add element at specified index.
	** Start from 1.
	*/
	public void add(int index, E e)
	{
		DNode<E> node  = new DNode<E>(e);
		if(isEmpty()) // List is empty
		{
			head = node;
		}
		else if(index == 1) // Insert at front
		{
			head.prev = node;
			node.next = head;
			head = node;
		}
		else // Insert elsewhere
		{
			DNode<E> runner = head;
			while(index > 1 && runner != null)
			{
				runner = runner.next;
				index--;
			}
			runner.prev.next = node;
			node.prev = runner.prev;
			node.next = runner;
			runner.prev = node;
		}
	}
	/*
	** Clear entire list.
	** Could go through list and set each
	** element to null.
	*/
	public void clear()
	{
		head = null;
	}
	// Return element at index
	public E get(int index)
	{
		DNode<E> runner = head;
		while(index != 1 && runner != null)
		{
			runner = runner.next;
		}
		return runner.e;
	}
	// Return index of element
	public int indexOf(Object o)
	{
		return -1;
	}
	// Remove and return element at index
	public E remove(int index)
	{
		E e;
		if(index == 1)
		{
			e = removeFirst();
		}
		else
		{
			DNode<E> runner = head;
			while(index != 1 && runner != null)
			{
				runner = runner.next;
				index--;
			}
			e = runner.e; // Value to return
			if(runner.next == null) // Last element
			{
				runner.prev.next = null;
			}
			else // Middle
			{
				runner.prev.next = runner.next;
				runner.next.prev = runner.prev;
			}
		}

		return e;
	}
	// Remove and return first element
	public E removeFirst()
	{
		DNode<E> first = head;
		E e = first.e;
		head = first.next;
		first.next.prev = null;
		first.next = null;
		first = null;
		return e;
	}
	// Set element at index to value e
	public E set(int index, E e)
	{
		DNode<E> runner = head;
		while(index != 1 && runner != null)
		{
			runner = runner.next;
			index--;
		}
		runner.e = e;
		return e;
	}
	// Return the size of the list
	public int size()
	{
		int size = 0;
		DNode<E> runner = head;
		while(runner != null)
		{
			runner = runner.next;
			size++;
		}

		return size;
	}
	public boolean isEmpty()
	{
		return head == null;
	}
	// Print contents of the list starting from head
	public void print()
	{
		if(isEmpty()) return;
		DNode<E> runner = head;
		System.out.print(
			"[ " + runner.e + " ]");
		while(runner.next != null)
		{
			runner = runner.next;
			System.out.print(
				" -> [ " + runner.e + " ]");
		}
		System.out.println(); // Newline
	}
}