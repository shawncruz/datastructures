public interface MaxPriorityQueueADT<E> extends PriorityQueueADT<E>
{
	E deleteMax();
	E getMax();
}