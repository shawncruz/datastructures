public class ArrayQueue<E> implements QueueADT<E>
{
	// Initial size constant
	private static final int INIT_SIZE = 5;
	// Size of array
	private int SIZE;
	// Array that will be used as a circular array
	private Object[] q;
	// Number of elements in Q
	private int elementCount;
	// Pointers to front and rear index of Q
	private int front;
	private int rear;

	// Parameterized constructor taking in initial size.
	public ArrayQueue(int size)
	{
		this.SIZE = size;
		q = new Object[size];
		front = rear = 0;
	}
	// Default constructor.
	public ArrayQueue()
	{
		this(INIT_SIZE);
	}

	// Add element to the rear.
	public void enqueue(E e)
	{
		// Check if Q has is full
		if(elementCount == SIZE)
			resize();

		q[rear] = e;
		rear = (rear + 1)%SIZE;
		elementCount++;
	}
	// Remove element from the front.
	public E dequeue()
	{
		if(isEmpty()) return null; // Nothing to d-Q

		E frontElement = (E) q[front];
		front = (front + 1)%SIZE;
		elementCount--;
		return frontElement;
	}
	// Resize Q
	private void resize()
	{
		SIZE *= 2;
		// Double size of array
		Object temp[] = new Object[SIZE];
		for(int i = front; i < elementCount; i = (i + 1)%(SIZE/2))
		{
			temp[i] = q[i];
		}
		q = temp;
		front = 0;
		rear = elementCount;
	}
	// Peak at the front element.
	public E front()
	{
		return (E) q[front];
	}
	// Return number of elements in Q.
	public int size()
	{
		return (SIZE + rear - front)%SIZE;
	}
	// Check if the size of the Q is zero.
	public boolean isEmpty()
	{
		return elementCount == 0;
	}
}