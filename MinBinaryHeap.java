public class MinBinaryHeap<E> extends BinaryHeap<E> implements MinPriorityQueueADT<E>
{
	/*
	*	Constructor with user-specified capacity
	*/
	public MinBinaryHeap(int capacity)
	{
		super(capacity);
	}

	/*
	*	Insert element
	*/
	public E insert(E element)
	{
		return null;
	}

	/*
	*	Return the maximum element in the heap
	*/
	public E getMin()
	{
		if(count == 0)
			return null;

		return array[0];
	}

	/*
	*	Remove and return the minimum element in the heap
	*/
	public E deleteMin()
	{
		return null;
	}

	/*
	*	Percolate element downward
	*/
	protected void percolateDown(int index)
	{

	}

	/*
	*	Percolate element upward
	*/
	protected void percolateUp(int index)
	{

	}
}