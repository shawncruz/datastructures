/* App class tests all of the data structures.
*/
public class App
{
	public static void main(String[] args)
	{
		ArrayQueue<String> q = new ArrayQueue<>();
		ArrayStack<String> s = new ArrayStack<>();
		q.enqueue("Sade");
		q.enqueue("is");
		q.enqueue("so");
		q.enqueue("beautiful");
		// This sequence of while loops reverses the queue
		while(!q.isEmpty())
		{
			print("size: " + q.size());
			String elt = q.dequeue();
			s.push(elt);
			print("front: " + elt);
		}
		while(!s.isEmpty())
		{
			String elt = s.pop();
			q.enqueue(elt);
			print("top: " + elt);
		}
		while(!q.isEmpty())
		{
			String elt = q.dequeue();
			print("front: " + elt);
		}
	}
	// Short-hand println
	public static void print(Object o)
	{
		System.out.println(o);
	}
}