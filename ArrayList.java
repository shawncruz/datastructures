public class ArrayList<E> implements ListADT<E>
{
	private Object[] list;
	private int size;
	// Initial size of list
	private static final int DEFAULT = 10;

	/**
	* Creates an empty list with the specified initial capacity.
	*/
	public ArrayList(int initialCapacity)
	{
		list = new Object[initialCapacity];
		size = 0;
	}

	/**
	* Creates an empty list of initial capacity of DEFAULT.
	*/
	public ArrayList()
	{
		this(DEFAULT);
	}

	public int size()
	{
		return size;
	}

	// Adds an element at the end of the list
	public void add(E element)
	{
		ensureCapacity(size);
		list[size++] = element;
	}

	private void ensureCapacity(int size)
	{
		if(list.length - 1 < size)
		{
			resize();
		}
	}

	// Resizes list to twice the amount of elements in the list.
	private void resize()
	{
		// May not work
		Object[] copy = list;
		list = new Object[2*size];
		for(int i = 0; i < copy.length; i++)
		{
			list[i] = copy[i];
		}
	}

	// Adds an element at a specified index.
	public void add(int index, E element)
	{
		rangeCheckForAdd(index);
		// Move everything to make room for new element
		for(int i = size + 1; i > index; i--)
			list[i] = list[i - 1];
		// Insert new element
		list[index] = element;
		size++;
	}

	// Clears every element in list.
	public void clear()
	{
		for(int i = 0; i < size; i++)
			list[i] = null;

		size = 0;
	}

	private void rangeCheckForAdd(int index)
	{
		if(index > size || index < 0)
			throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
	}

	private void rangeCheck(int index)
	{
		if (index >= size)
			throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
	}

	private String outOfBoundsMsg(int index)
	{
		return "Index: " + index + ", Size: " + size;
	}

	// Return the element in the list at the specified index
	public E get(int index)
	{
		rangeCheck(index);
		return (E) list[index]; // ERROR CHECK
	}

	/**
	* If found return index, else -1;
	*/
	public int indexOf(Object o)
	{
		for(int i = 0; i < size; i++)
			if(o.equals(list[i]))
				return i;

		return -1;
	}

	public E remove(int index)
	{
		rangeCheck(index);

		E removedValue = (E) list[index];
		for(int i = index; i < size; i++)
			list[i] = list[i + 1];

		size--;
		return removedValue;
	}

	public E set(int index, E element)
	{
		rangeCheck(index);

		E oldValue = (E) list[index];
		list[index] = element;
		return oldValue;
	}
}