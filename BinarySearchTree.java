public class BinarySearchTree<E1 extends E> extends BinaryTree<E> implements BinarySearchTreeADT<E>
{
	public static void main(String[] args)
	{
		BinarySearchTreeADT<String> tree = new BinarySearchTree<>();
		for(int i = 0; i < 100; i+=6)
			tree.insert("" + i);
		System.out.println("max: " + tree.findMax());
	}
	/*******************************************************
	*
	*  Insert
	*
	********************************************************/
	/*
	** Overwrites the insert method in parent class; adheres to BST rules.
	*/
	public void insert(E element)
	{
		root = insert(root, element);
	}
	private BTNode insert(BTNode node, E element)
	{
		if(node == null)
			node = new BTNode(element);
		else if(element.compareTo(node.data) == -1) // Go left
			node = insert(node.left, element);
		else
			node = insert(node.right, element);
		return root;
	}

	/*******************************************************
	*
	*  Find
	*
	********************************************************/
	// Find the element in the tree if it exists
	public E find(E element)
	{
		return find(root,element);
	}
	// If found, return data -- else, return null
	private E find(BTNode node, E element)
	{
		while(node != null)
		{
			if(element.compareTo(node.data) == -1) // Go left
				find(node.left, element);
			else if(element.compareTo(node.data) == 1) // Go right
				find(node.right, element);
			else
				return node.data;
		}
		return null;
	}

	// Finds min elemeht in tree
	public E findMin()
	{
		return findMin(root);
	}
	private E findMin(BTNode node)
	{
		if(node == null)
			return null;

		while(node.left != null)
			node = node.left;

		return node.data;
	}

	// Finds max elemeht in tree
	public E findMax()
	{
		return findMin(root);
	}
	private E findMax(BTNode node)
	{
		if(node == null)
			return null;

		while(node.right != null)
			node = node.right;

		return node.data;
	}

	/*******************************************************
	*
	*  Delete
	*
	********************************************************/
	// Delete an element from the tree
	public E delete(E element)
	{
		return delete(root, element).data;
	}
	private BTNode delete(BTNode node, E element)
	{
		if(node == null)
			return null;

		if(element.compareTo(node.data) == -1) // Go left
			node = delete(node.left, element);
		else if(element.compareTo(node.data) == 1) // Go right
			node = delete(node.right, element);
		else // Found
		{
			if(node.left != null && node.right != null) // Node has two children.
			{
				node = findMax(node.left);
				node.left = delete(node.left, node.data);
			}
			else // One or no children
			{
				if(node.left != null)
					node = node.left;
				else if(node.right != null)
					node = node.right;
			}
		}
		return node;
	}
}